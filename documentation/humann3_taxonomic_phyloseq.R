#For this file to work you need to change all the paths to your own data paths were the right files are located

#Preparation
library("readr")
library("tidyverse")
library("phyloseq")
#Set working directory (change to your personal folder)
setwd("T:/microbiologie/Research/Microbiota/Personal folders/Jari Koot")


#_____________________________________________________Humann2 import_____________________________________________________#
#Load the data from the Humann2 pipeline (change the path to were the Humann2 output file is located)
humann_output <- read_delim("Rstudio/data/merged_metaphlan_profiles_w_reads_stats.txt", delim = "\t", skip = 1, escape_double = FALSE, trim_ws = TRUE)
#optional step, exclude unclassified and unknown samples (when skipping these lines you can encounter an error:  duplicate 'row.names' are not allowed)

humann_output <- humann_output %>% filter(clade_name != "unclassified")
humann_output <- humann_output %>% filter(clade_name != "UNKNOWN")

#_____________________________________________________TAX table_____________________________________________________#
#Creating the tax table
tax_table_import <- humann_output %>% select("clade_taxid" ,"clade_name") 
tax_table_import
#tax_table_import <- tax_table_import %>% dplyr::rename("FeatureID" = "clade_taxid")
#split the taxonomic columns (each tax has its own column)
tax_table_seperate <- separate(data = tax_table_import, col = clade_name, into = c("Kingdom", "Phylum", "Class", "Order", "Family", "Genus", "Species"), sep = "\\|.__") %>% na.omit()
tax_table_seperate
tax_table_seperate$Kingdom <- gsub("k__", "", as.character(tax_table_seperate$Kingdom))
#Convert column to rowname
tax_table <- column_to_rownames(tax_table_seperate, var = "clade_taxid") %>% as.matrix()
#Make the tax table phyloseq ready
PHY_tax_table <- tax_table(tax_table)
#Optional step: remove NA values from the tax table
PHY_tax_table <- na.omit(PHY_tax_table)

#_____________________________________________________OTU table_____________________________________________________#
#Creating the otu table
OTU_data <- humann_output %>% select(!"clade_name")
#Rewrite the sample ID's 
#Example: S0003_metaphlan_w_read_stats -> s0003
colnames(OTU_data) <- sub("_metaphlan_w_read_stats", "", colnames(OTU_data))
head(OTU_data)
#Convert column to rownames
OTU_data <- column_to_rownames(OTU_data, var = "clade_taxid")
OTU_data
is.numeric(OTU_data)
class(OTU_data)
#Make the sample data phyloseq ready
PHY_otu_table <- otu_table(OTU_data, taxa_are_rows = TRUE)
PHY_otu_table
#_____________________________________________________Sample data_____________________________________________________#
#import the sample data file
sample_data <-  read_delim("Rstudio/data/Bacterium/Mapping_file_mrd.txt", "\t", escape_double = FALSE, trim_ws = TRUE)
#convert column to rownames
sample_data <- column_to_rownames(sample_data, var = "SampleID")
#Make the sample data phyloseq ready
PHY_sample_data = sample_data(sample_data)



                                                                                      
#Make the phyloseq object
phyloseq <- merge_phyloseq(PHY_otu_table, PHY_tax_table, PHY_sample_data)
phyloseq



